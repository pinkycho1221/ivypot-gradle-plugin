//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2020
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.ivypot.internal

import groovy.transform.CompileStatic
import org.gradle.api.GradleException
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ResolvedArtifact
import org.ysb33r.gradle.ivypot.remote.IvyDependency

import javax.annotation.Nullable

/**
 *
 * @since 0.12
 */
@CompileStatic
class ResolvedDependencyProxy implements Dependency {

    ResolvedDependencyProxy(ResolvedArtifact artifact, boolean transitive) {
        this.resolved = artifact
        this.transitive = transitive
    }

    @Override
    String getGroup() {
        resolved.moduleVersion.id.group
    }

    @Override
    String getName() {
        resolved.moduleVersion.id.name
    }

    @Override
    String getVersion() {
        resolved.moduleVersion.id.version
    }

    @Override
    boolean contentEquals(Dependency dependency) {
        return false
    }

    @Override
    Dependency copy() {
        throw new GradleException('This object cannot be copied')
    }

//    String getClassifier() {
//        resolved.classifier
//    }
//
//    String getExtension() {
//        resolved.extension
//    }
//
//    String getType() {
//        resolved.type
//    }

    final String reason = null

    @Override
    void because(@Nullable String s) {
    }

    IvyDependency asIvyDependency() {
        new IvyDependency(
            organisation:group,
            module: name,
            revision: version,
            transitive: transitive,
            typeFilter: resolved.type ?: '*',
            confFilter: '*',
            classifier: resolved.classifier,
            extension: resolved.extension
        )

    }
    private final ResolvedArtifact resolved
    private final boolean transitive
}
