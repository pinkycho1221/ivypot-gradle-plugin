//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2013-2020
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.ivypot

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.IgnoreIf
import spock.lang.Issue
import spock.lang.Specification

// This is strongly based upon https://gist.github.com/tr7zw/debb0758e68dcc107024c361c7440503
@IgnoreIf({ System.getProperty('IS_OFFLINE') })
class MultiProjectIntegrationSpec extends Specification {

    public static final String REPO_PATH = 'repo'
    public static final String DEFAULT_TASK = 'syncRemoteRepositories'

    @Rule
    TemporaryFolder testFolder

    File projectDir
    File buildFile
    File settingsFile
    File repoDir
    File subprojectDir

    void setup() {
        projectDir = testFolder.root
        buildFile = new File(projectDir, 'build.gradle')
        settingsFile = new File(projectDir, 'settings.gradle')
        repoDir = new File(projectDir, REPO_PATH)
        settingsFile.text = 'rootProject.name="testproject"'
        subprojectDir = new File(projectDir, 'subproject')

        writeSettingsGradle()
        writeMainBuildGradle()
        writeSubprojectGradle()
    }

    @Issue(['https://gitlab.com/ysb33rOrg/ivypot-gradle-plugin/-/issues/21',
        'https://gitlab.com/ysb33rOrg/ivypot-gradle-plugin/-/issues/31'
    ])
    void 'Dependency resolution with default dependencies'() {
        when:
        BuildResult result = build()

        then:
        result.task(":subproject:${DEFAULT_TASK}").outcome == TaskOutcome.SUCCESS
        file_exists('org.projectlombok/lombok/1.18.10/lombok-1.18.10.jar')
    }

    void writeMainBuildGradle() {
        buildFile.text = '''
plugins {
    id 'org.ysb33r.ivypot' apply false
}

subprojects {
    apply plugin: 'java-library'
    apply plugin : 'org.ysb33r.ivypot'

    group = 'somegroup'

    dependencies {
        constraints {
            // Define dependency with version to be used.
            // This version is used when we define a dependency
            // without a version.
            annotationProcessor("org.projectlombok:lombok:1.18.10")
            implementation("org.projectlombok:lombok:1.18.10")
        }
    }

    repositories {
        mavenCentral()
    }

    syncRemoteRepositories {
      repoRoot "${rootDir}/repo"
    
      repositories {
        mavenCentral()
      }

        configurations 'implementation'

        includeBuildScriptDependencies = false
    }
}
'''
    }

    void writeSettingsGradle() {
        settingsFile.text = """
rootProject.name = 'rootproject'
include ":subproject"
"""
    }

    void writeSubprojectGradle() {
        subprojectDir.mkdirs()
        new File(subprojectDir, 'build.gradle').text = """
dependencies {
    annotationProcessor group: 'org.projectlombok', name:'lombok'
    implementation group: 'org.projectlombok', name:'lombok'
}

version = '1.0.0'
"""
    }

    private BuildResult build() {
        GradleRunner.create().withDebug(true)
            .withPluginClasspath()
            .withProjectDir(projectDir)
            .withArguments(['-i', '-s', DEFAULT_TASK])
            .forwardOutput()
            .build()
    }

    private boolean file_exists(String path) {
        new File(repoDir, path).exists()
    }


}